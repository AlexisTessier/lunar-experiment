var Webgl = (function(){

    function Webgl(width, height, container, renderFunction){
        // Basic three.js setup
        this.scene = new THREE.Scene();
        this.scene.fog = new THREE.FogExp2( 0x000000, 0.00000025 );
        
        this.camera = new THREE.PerspectiveCamera(50, width / height, 1, 10000);
        this.camera.position.z = 800;

        this.currentCameraMove = {
            origin : {
                move : new THREE.Vector3(this.camera.position.x, this.camera.position.y, this.camera.position.z),
                look : new THREE.Vector3(0, 0, 0)
            },
            movement : {
                move : new THREE.Vector3(0, 0, 0),
                look : new THREE.Vector3(0, 0, 0)
            },
            previousLookAt : new THREE.Vector3(0, 0, 0)
        };

        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setSize(width, height);
        this.renderer.setClearColor(0x2D2D2D);

        this.objectList = [];
        this.indexedObjects = {};
        this.objectGroup = {};

        this.renderFunction = false;
        this.setRenderFunction(renderFunction);

        var light = new THREE.AmbientLight( 0x404040 ); // soft white light
        this.scene.add( light );

        var pointLight = new THREE.PointLight( 0xffffff, 1, 3000 );
        pointLight.position.set(0, 300, 500 );
        this.scene.add( pointLight );

        /*var sphereSize = 1;
        var pointLightHelper = new THREE.PointLightHelper( pointLight, sphereSize );
        this.scene.add( pointLightHelper );*/

        /*------------------*/
        /*this.controls = new THREE.TrackballControls(this.camera);

        this.controls.rotateSpeed = 1.0;
        this.controls.zoomSpeed = 1.2;
        this.controls.panSpeed = 0.8;

        this.controls.noZoom = false;
        this.controls.noPan = false;

        this.controls.staticMoving = true;
        this.controls.dynamicDampingFactor = 0.3;

        this.controls.keys = [ 65, 83, 68 ];*/

        //this.controls.addEventListener( 'change', this.renderFunction);
        /*--------------------*/

        this.startTime = new Date().getTime();
        this.previousTime = this.startTime;
        this.currentTime = this.startTime;
        this.deltaTime = this.currentTime - this.previousTime
        this.totalTime = 0 + this.deltaTime;

        //definition de la skybox
        var skyboxName = 'grey';
        var urlsSkybox = [
          'textures/left'+skyboxName+'.jpg',
          'textures/right'+skyboxName+'.jpg',
          'textures/front'+skyboxName+'.jpg',
          'textures/back'+skyboxName+'.jpg',
          'textures/top'+skyboxName+'.jpg',
          'textures/bot'+skyboxName+'.jpg'
        ];

        var cubemap = THREE.ImageUtils.loadTextureCube(urlsSkybox); // load textures
        cubemap.format = THREE.RGBFormat;

        var shader = THREE.ShaderLib['cube']; // init cube shader from built-in lib
        shader.uniforms['tCube'].value = cubemap; // apply textures to shader

        // create shader material
        var skyBoxMaterial = new THREE.ShaderMaterial( {
          fragmentShader: shader.fragmentShader,
          vertexShader: shader.vertexShader,
          uniforms: shader.uniforms,
          depthWrite: false,
          side: THREE.BackSide
        });

        // create skybox mesh
        this.skybox = new THREE.Mesh(
          new THREE.CubeGeometry(1000, 1000, 1000),
          skyBoxMaterial
        );

        //this.skybox.position.set(0,0,0);

        this.scene.add(this.skybox);

        this.periods = {};
    }

    Webgl.prototype.setTime = function() {
        this.currentTime = new Date().getTime();
        this.deltaTime = this.currentTime - this.previousTime;
        this.totalTime += this.deltaTime;
        this.previousTime = this.currentTime;
    };

    Webgl.prototype.resize = function(width, height) {
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(width, height);
    };

    Webgl.prototype.group = function(groupName) {
        var that = this;

        if(_(groupName).isString() && (''+groupName).length > 0){
            if (!_(that.objectGroup[groupName]).isObject()) {
                that.objectGroup[groupName] = [];
            }

            return {
                add : function (objectOrId) {
                    if(_(objectOrId).isString()){
                        var objectToAdd = that.getObject(objectOrId);
                        if (objectToAdd) {
                            that.objectGroup[groupName].push({id : objectOrId, object : objectToAdd});
                        }

                        return objectToAdd;
                    }
                    else if(_(objectOrId).isArray()){
                        var group = this;
                        _(objectOrId).each( function( value, key, objectOrId ) {
                            group.add(value);
                        });
                    }
                    else if(_(objectOrId).isObject()){
                        that.objectGroup[groupName].push({id : false, object : objectOrId});

                        that.scene.add(objectOrId);

                        return objectOrId;
                    }
                }
            };
        }

        return false;
    };

    Webgl.prototype.addObject = function(object, id) {
        this.objectList.push(object);

        if(_(id).isString() && (''+id).length > 0){
            this.indexedObjects[id] = object;
        }

        this.scene.add(object);

        return object;
    };

    Webgl.prototype.getObject = function(id) {
        return (_(id).isString() && _(this.indexedObjects[id]).isObject()) ? this.indexedObjects[id] : false;
    };

    Webgl.prototype.setRenderFunction = function(renderFunction) {
        this.renderFunction = renderFunction;

        return this;
    };

    Webgl.prototype.render = function() {
        this.setTime();
        this.setPeriods();
        //this.controls.movementSpeed = 0.33;
//this.controls.update();

        this.setCameraMove();
        
        if(_(this.renderFunction).isFunction()){
            this.renderFunction(this, this.deltaTime);
        }
    };

    Webgl.prototype.addPeriod = function(name, params) {
        params = _(params).isObject() ? params : {};

        this.periods[name] = {
            reverse : _(params.reverse).isBoolean() ? params.reverse : false,
            ratio : 0,
            currentTime : 0,
            duration : _(params.duration).isNumber() ? params.duration : 0,
            loop : _(params.loop).isBoolean() ? params.loop : false,
            finish : false,
            start : false,
            easing : params.easing ? params.easing : 'linear',
            reverseState : false,
            easeFactor : 0,
            loopCount : 0,
            maxLoopCount : _(params.maxLoopCount).isNumber() ? params.maxLoopCount : false,
            endAction : params.endAction
        };
    };

    Webgl.prototype.getPeriod = function(name) {
        return this.periods[name];
    };

    Webgl.prototype.startPeriod = function(name) {
        this.periods[name].start = true;
    };

    Webgl.prototype.finishPeriod = function(name) {
        this.periods[name].finish = true;
    };

    Webgl.prototype.setPeriods = function() {
        for(var key in this.periods){
            var period = this.periods[key];

            if (!period.finish && period.start) {
                if (period.reverseState) {
                    period.ratio = period.currentTime < 0 ? 0 : period.currentTime/period.duration;
                }
                else{
                    period.ratio = period.currentTime > period.duration ? 1 : period.currentTime/period.duration;
                }
                
                var easingFunction = Easing[period.easing];
                period.easeFactor = easingFunction(period.ratio);

                var isEnded = period.reverseState ? (period.ratio === 0) : (period.ratio === 1);


                if(isEnded){
                    if (period.loop) {
                        if (period.maxLoopCount === false) {
                            period.finish = false;
                        }
                        else if(period.loopCount >= period.maxLoopCount){
                            period.finish = true;
                        }
                        else{
                            period.finish = false;
                        }
                    }
                    else{
                        period.finish = true;
                    }

                    if (period.finish && _(period.endAction).isFunction()) {
                        period.endAction(period);
                    }

                    if (period.loop) {
                        period.loopCount++;

                        if (period.reverse) {
                            period.currentTime = period.reverseState ? 0 : period.duration;
                        }
                        else{
                            period.currentTime = 0;
                        }
                        

                        period.reverseState = period.reverse ? !period.reverseState : period.reverseState;
                    }
                }
                else{
                    period.currentTime += period.reverseState ? -this.deltaTime : this.deltaTime;
                }
            }
        }
    };

    Webgl.prototype.moveCamera = function(params) {
        var params = _(params).isObject() ? params : {};

        var that = this;
        var position = _(params.position).isObject() ? params.position : new THREE.Vector3(0, 0, 800);
        var time = _(params.time).isNumber() ? params.time : 500;
        var lookAt = _(params.lookAt).isObject() ? params.lookAt : new THREE.Vector3(0, 0, 0);
        var easing = _(params.easing).isString() ? params.easing : 'linear';

        this.addPeriod('cameraMove', {
            easing : easing,
            duration : time
        });

        this.startPeriod('cameraMove');

        var diffX = position.x - that.camera.position.x;
        var diffY = position.y - that.camera.position.y;
        var diffZ = position.z - that.camera.position.z;

        var diffLookX = lookAt.x - that.currentCameraMove.previousLookAt.x;
        var diffLookY = lookAt.y - that.currentCameraMove.previousLookAt.y;
        var diffLookZ = lookAt.z - that.currentCameraMove.previousLookAt.z;

        this.currentCameraMove = {
            origin : {
                move : new THREE.Vector3(that.camera.position.x, that.camera.position.y, that.camera.position.z),
                look : new THREE.Vector3(that.currentCameraMove.previousLookAt.x, that.currentCameraMove.previousLookAt.y, that.currentCameraMove.previousLookAt.z)
            },
            movement : {
                move : new THREE.Vector3(diffX, diffY, diffZ),
                look : new THREE.Vector3(diffLookX, diffLookY, diffLookZ)
            },
            previousLookAt : new THREE.Vector3(that.currentCameraMove.previousLookAt.x, that.currentCameraMove.previousLookAt.y, that.currentCameraMove.previousLookAt.z)
        };
    };

    Webgl.prototype.setCameraMove = function() {
        var cameraMovement = this.getPeriod('cameraMove');
        if (cameraMovement) {
            if (cameraMovement.start && !cameraMovement.finish) {
                var posX = this.currentCameraMove.origin.move.x+(this.currentCameraMove.movement.move.x*cameraMovement.easeFactor);
                var posY = this.currentCameraMove.origin.move.y+(this.currentCameraMove.movement.move.y*cameraMovement.easeFactor);
                var posZ = this.currentCameraMove.origin.move.z+(this.currentCameraMove.movement.move.z*cameraMovement.easeFactor);

                this.camera.position.set(posX, posY, posZ);

                var lookX = this.currentCameraMove.origin.look.x+(this.currentCameraMove.movement.look.x*cameraMovement.easeFactor);
                var lookY = this.currentCameraMove.origin.look.y+(this.currentCameraMove.movement.look.y*cameraMovement.easeFactor);
                var lookZ = this.currentCameraMove.origin.look.z+(this.currentCameraMove.movement.look.z*cameraMovement.easeFactor);

                this.currentCameraMove.previousLookAt = new THREE.Vector3(lookX, lookY, lookZ);

                this.camera.lookAt(new THREE.Vector3(lookX, lookY, lookZ));
            }
        }
    };

    return Webgl;

})();