
THREE.ShaderLib['unstable'] = {
	uniforms: {
		endedImpact : {type : 'f', value : 0.0},
		impact : {type : 'f', value : 0.0},
		impactPosition : {type : 'v3', value : new THREE.Vector3(0, 0, 0)},
	},

	vertexShader: [
		"attribute float ratio;",
		"attribute float depthModif;",
		"varying float depth;",
		"uniform vec3 impactPosition;",
		"uniform float impact;",

		"void main() {",
			"float distanceX = impactPosition.x > position.x  ? impactPosition.x - position.x : position.x - impactPosition.x;",
			"float distanceY = impactPosition.y > position.y  ? impactPosition.y - position.y : position.y - impactPosition.y;",
			"float distance = (distanceX + distanceY)*0.5;",
			"float distanceFactor = 0.4;",
			"if (impact == 1.0){",
				"if (distance < 200.0){",
					"distanceFactor += 0.10;",
				"}",
				"if (distance < 175.0){",
					"distanceFactor += 0.25;",
				"}",
				"if (distance < 150.0){",
					"distanceFactor += 0.38;",
				"}",
				"if (distance < 125.0){",
					"distanceFactor += 0.45;",
				"}",
				"if (distance < 100.0){",
					"distanceFactor += 0.60;",
				"}",
			"}",
			"depth = 4.0+(5.-(position.z+depthModif))*ratio*distanceFactor;",
			"gl_Position = projectionMatrix * modelViewMatrix * vec4(position.x, position.y, depth*7.0, 1.0 );",
		"}"

	].join("\n"),

	fragmentShader: [
		"varying vec3 fNormal;",
		"vec3 color;",
		"varying float depth;",

		"void main() {",
			//"color = vec3(0.9333, 0.6039, 0.1764);",
			"color = vec3(0.75, 0.75, 0.75);",
			"gl_FragColor = vec4(color*depth*0.08, 1.0);",
		"}"

	].join("\n")
};