var getShaderNamed = function (shaderName, attributes) {
	var currentShader = {
		uniforms : _(THREE.ShaderLib[shaderName].uniforms).clone(),
		vertexShader : THREE.ShaderLib[shaderName].vertexShader,
		fragmentShader : THREE.ShaderLib[shaderName].fragmentShader
	};

	if (_(attributes).isObject()) {
		currentShader.attributes = attributes;
	}

	return currentShader;
};