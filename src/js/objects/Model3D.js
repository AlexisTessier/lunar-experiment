var Model3D = (function(){
    function Model3D(){
        THREE.Object3D.call(this);

        this.anim = {
            appear : {
                start : false,
                finish : false,
                currentTime : 0,
                duration : 400,
                loop : false,
                easing : 'outBounce',
                delay : 750,
                afterDelayAction : false,
                endAction : false,
                action : function(object, easeFactor){
                    if (!object.mesh.visible) {
                        object.show();
                    }

                    if (easeFactor === 0) {
                        easeFactor += 0.001;
                    }

                    object.mesh.scale.set(easeFactor/2, easeFactor/2, easeFactor/2);
                }
            }
        };
    }

    Model3D.prototype = new THREE.Object3D;
    Model3D.prototype.constructor = Model3D;

    Model3D.prototype.init = function(geometry, material) {
        this.mesh = new THREE.Mesh(geometry, material);
        this.add(this.mesh);
    };

    Model3D.prototype.getParams = function(params, defaultParams) {
        var defaultParams = _(defaultParams).clone();

        if (_(params).isObject()) {
            for(var key in defaultParams){
                var currentParam = params[key];
                if (!_(currentParam).isUndefined()) {
                    defaultParams[key] = currentParam;
                }
            }
        }

        return defaultParams;
    };

    Model3D.prototype.rotate = function(x, y, z) {
        this.mesh.rotation.x += _(x).isNumber() ? x : 0;
        this.mesh.rotation.y += _(y).isNumber() ? y : 0;
        this.mesh.rotation.z += _(z).isNumber() ? z : 0;
    };

    Model3D.prototype.startAnim = function(name, callbackAfterDelay, callback) {
        this.anim[name].start = true;
        this.anim[name].afterDelayAction = callbackAfterDelay;
        this.anim[name].endAction = callback;
    };

    Model3D.prototype.update = function(deltaTime) {
        for(var key in this.anim){

            var anim = this.anim[key];

            if (!anim.finish && anim.start) {
                if (anim.currentTime > anim.delay) {
                    if(_(anim.afterDelayAction).isFunction()){
                        anim.afterDelayAction(key, this);
                        anim.afterDelayAction = null;
                    }

                    var realCurrentTime = anim.currentTime - anim.delay;

                    var ratioTime = realCurrentTime > anim.duration ? 1 : realCurrentTime/anim.duration;
                    var easingFunction = Easing[anim.easing];
                    var easeFactor = easingFunction(ratioTime);

                    anim.action(this, easeFactor);
                }

                if(ratioTime == 1){
                    anim.finish = !anim.loop;

                    if (anim.loop) {
                        anim.currentTime = 0;
                    }
                    else{
                        if (_(anim.endAction).isFunction()) {
                            anim.endAction(key, this);
                            anim.endAction = null;
                        }
                    }
                }
                else{
                    anim.currentTime += deltaTime;
                }
            }
        }
    };

    Model3D.prototype.hide = function() {
        this.mesh.visible = false;
    };

    Model3D.prototype.show = function() {
        this.mesh.visible = true;
    };

    return Model3D;
})();