var Model2D = (function() {

	'use strict';

	function Model2D(domElem) {
		// enforces new
		if (!(this instanceof Model2D)) {
			return new Model2D(domElem);
		}
		
		this.domElem = domElem;

		this.clickEvent = false;

		this.init();
	}

	Model2D.prototype.init = function() {
		this.setSize();
		this.bindEvents();
	};

	Model2D.prototype.setSize = function(params) {
		params = _(params).isObject() ? params : {};

		this.currentWidth = params.width ? params.width : this.getOriginWidth();
		this.currentHeight = params.width ? params.width : this.getOriginHeight();

		this.domElem.width(this.currentWidth);
		this.domElem.height(this.currentHeight);
	};

	Model2D.prototype.getModelType = function() {
		return this.domElem.attr('data-model-type');
	};

	Model2D.prototype.getID = function() {
		return this.domElem.attr('id');
	};

	Model2D.prototype.getOriginHeight = function() {
		return parseInt(this.domElem.attr('data-height'));
	};

	Model2D.prototype.getOriginWidth = function() {
		return parseInt(this.domElem.attr('data-width'));
	};

	Model2D.prototype.click = function(callback) {
		this.clickEvent = callback;
	};

	Model2D.prototype.bindEvents = function() {
		var that = this;

		this.domElem.click(function (evt) {
			if (_(that.clickEvent).isFunction()) {
				that.clickEvent(that);
			}
		});
	};

	Model2D.prototype.init3DEquivalent = function() {
		return new Sphere({rayon : this.currentWidth/2});
	};

	Model2D.prototype.hide = function() {
		this.domElem.addClass('hidden');
	};

	Model2D.prototype.show = function() {
		this.domElem.removeClass('hidden');
	};

	Model2D.prototype.pushIn3D = function() {
		this.domElem.addClass('pushed');
	};

	Model2D.prototype.comeBackIn2D = function() {
		this.domElem.removeClass('pushed');
	};

	Model2D.prototype.update = function(deltaTime) {
	};

	return Model2D;
}());