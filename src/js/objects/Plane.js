var Plane = (function(){
    var defaultParams = {
        width : 120,
        height : 120,
        widthSegments : 10,
        heightSegments : 10,
        wireframe : true,
        color : 0x3facc8,
        textureName : false
    };

    function Plane(params){
        Model3D.call(this, params);

        var params = this.getParams(params, defaultParams);

        var geometry = new THREE.PlaneGeometry(params.width, params.height, params.widthSegments, params.heightSegments);
        var material = null;

        this.periodsForVertices = [];
        this.periodsConstantForVertices = [];

        if (params.textureName) {
            var texture = THREE.ImageUtils.loadTexture("./textures/"+params.textureName);
            texture.wrapS = texture.wrapT = THREE.RepeatWrapping;

            var attributes = {
                depthModif:{ type: 'f', value: []},
                ratio : {type : 'f', value : []}//,
                //impactModif : {type : 'f', value : []}
            };

            for(var i = 0; i < geometry.vertices.length; i++) {
                attributes.depthModif.value[i] = 0.1*_.random(0, 40);
                attributes.ratio.value[i] = 0.6;
               // attributes.impactModif.value[i] = 1.0;

                //attributes.impact.value[i] = 0.0;
                var periodIndex = _.random(1, 5);
                this.periodsForVertices[i] = webgl.getPeriod('unstable'+periodIndex);
                periodIndex = _.random(1, 5);
                this.periodsConstantForVertices[i] = webgl.getPeriod('unstableConstant'+periodIndex);
            }
            attributes.depthModif.needsUpdate = true;
            attributes.ratio.needsUpdate = true;
            //attributes.impactModif.needsUpdate = true;

            material = new THREE.ShaderMaterial(getShaderNamed('unstable', attributes));
        }
        else{
            material = new THREE.MeshBasicMaterial({color: params.color, wireframe: params.wireframe, side: THREE.DoubleSide});
        }

        this.init(geometry, material);
    }

    Plane.prototype = new Model3D;
    Plane.prototype.constructor = Plane;

    Plane.prototype.update = function(deltaTime) {
        Model3D.prototype.update.apply(this, [deltaTime]);
    };

    return Plane;
})();