var Easing = {
  linear : function (t){
    return t;
  },
  inQuad : function (t){
    return t * t;
  },
  outQuad : function (t){
    return -1 * t * (t -2);
  },
  inOutQuad : function (t){
    t = t / 2;
    return (t * t) / 2;
  },
  inCubic : function (t){
    return t * t * t;
  },
  outCubic : function (t){
    t = t - 1;
    return t * t * t + 1;
  },
  inOutCubic : function (t){
    t = t / 2;
    return (t * t * t) / 2;
  },
  inQuart : function (t){
    return t * t * t * t;
  },
  outQuart : function (t){
    t = t - 1;
    return -1 * (t * t * t * t - 1);
  },
  inOutQuart : function (t){
    t = t / 2;
    return (t * t * t * t) / 2;
  },
  inQuint : function (t){
    return t * t * t * t * t;
  },
  outQuint : function (t){
    t = t - 1;
    return (t * t * t * t * t + 1);
  },
  inOutQuint : function (t){
    t = t / 2;
    return (t * t * t * t * t) / 2;
  },
  inSine : function (t){
    return -1 * Math.cos(t * (Math.PI / 2)) + 1;
  },
  outSine : function (t){
    return Math.sin(t * (Math.PI / 2));
  },
  inOutSine : function (t){
    return (Math.cos(Math.PI * t) - 1) / -2;
  },
  inExpo : function (t){
    return Math.pow(2, 10 * (t - 1));
  },
  outExpo : function (t){
    return -Math.pow(2, -10 * t) + 1;
  },
  inOutExpo : function (t){
    t = t / 2;
    return (Math.pow(2, 10 * (t - 1))) / 2;
  },
  inCirc : function (t){
    return -1 * (Math.sqrt(1 - t * t) - 1);
  },
  outCirc : function (t){
    t = t - 1;
    return Math.sqrt(1 - t * t);
  },
  inOutCirc : function (t){
    t = t / 2;
    return (Math.sqrt(1 - t * t) - 1) / -2;
  },
  inBack : function (t, overshoot){
    if (!overshoot && overshoot !== 0){
      overshoot = 1.70158;
    }
    return 1 * t * t * ( (overshoot + 1) * t - overshoot );
  },
  outBack : function (t, overshoot){
    if(!overshoot && overshoot !== 0){
      overshoot = 1.70158;
    }
    t = t - 1;
    return t * t * ((overshoot + 1) * t + overshoot) + 1;
  },
  inOutBack : function (t, overshoot){
    if(!overshoot && overshoot !== 0){
      overshoot = 1.70158;
    }
    t = t / 2;
    overshoot = overshoot * 1.525;
    return (t * t * ((overshoot + 1) * t - overshoot)) / 2;
  },
  inBounce : function (t){
    return 1 - Easing.outBounce(1 - t);
  },
  outBounce : function (t){
    if (t < 0.36363636363636365) {
      return 7.5625 * t * t;
    } else if (t < 0.7272727272727273) {
      t = t - 0.5454545454545454;
      return 7.5625 * t * t + 0.75;
    } else if (t < 0.9090909090909091) {
      t = t - 0.8181818181818182;
      return 7.5625 * t * t + 0.9375;
    } else {
      t = t - 0.9545454545454546;
      return 7.5625 * t * t + 0.984375;
    }
  },
  inOutBounce : function (t){
    if (t < 0.5){
      return Easing.inBounce (t*2) * 0.5;
    }
    return Easing.outBounce ( t*2-1 ) * 0.5 + 1 * 0.5;
  },
  inElastic : function (t, amplitude, period){
    var offset;
    // escape early for 0 and 1
    if (t === 0 || t === 1) {
      return t;
    }
    if (!period){
      period = 0.3;
    }
    if (!amplitude){
      amplitude = 1;
      offset = period / 4;
    } else {
      offset = period / (Math.PI * 2.0) * Math.asin(1 / amplitude);
    }
    t = t - 1;
    return -(amplitude * Math.pow(2,10 * t) * Math.sin(((t - offset) * (Math.PI * 2)) / period ));
  },
  outElastic : function (t, amplitude, period){
    var offset;
    // escape early for 0 and 1
    if (t === 0 || t === 1) {
      return t;
    }
    if (!period){
      period = 0.3;
    }
    if (!amplitude){
      amplitude = 1;
      offset = period / 4;
    } else {
      offset = period / (Math.PI * 2.0) * Math.asin(1 / amplitude);
    }
    return amplitude * Math.pow(2,-10 * t) * Math.sin( (t - offset) * ( Math.PI * 2 ) / period ) + 1;
  },
  inOutElastic : function (t, amplitude, period){
    var offset;
    t = (t / 2) - 1;
    // escape early for 0 and 1
    if (t === 0 || t === 1) {
      return t;
    }
    if (!period){
      period = 0.44999999999999996;
    }
    if (!amplitude){
      amplitude = 1;
      offset = period / 4;
    } else {
      offset = period / (Math.PI * 2.0) * Math.asin(1 / amplitude);
    }
    return (amplitude * Math.pow(2, 10 * t) * Math.sin((t - offset) * (Math.PI * 2) / period )) / -2;
  }
};