var Sphere = (function(){
    var defaultParams = {
        rayon : 1,
        wireframe : false,
        color : 0x666666
    };

    function Sphere(params){
        Model3D.call(this, params);

        var params = this.getParams(params, defaultParams);

        var geometry = new THREE.SphereGeometry(params.rayon);
        var material = new THREE.MeshLambertMaterial({
            color: params.color,
            wireframe: params.wireframe
        });

        this.init(geometry, material);
    }

    Sphere.prototype = new Model3D;
    Sphere.prototype.constructor = Sphere;

    Sphere.prototype.update = function(deltaTime) {
        Model3D.prototype.update.apply(this, [deltaTime]);

        this.mesh.rotation.z -= (deltaTime*0.0005);
        this.mesh.position.z += (deltaTime*0.05);
    };

    return Sphere;
})();