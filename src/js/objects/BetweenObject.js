var BetweenObject = (function() {
	'use strict';

	function BetweenObject(params) {
		// enforces new
		if (!(this instanceof BetweenObject)) {
			return new BetweenObject(model2D, model3D);
		}
		
		params = _(params).isObject() ? params : {};

		this.model2D = _(params.model2D).isObject() ? params.model2D : false;
		if (this.model2D) {
			this.state = '2D';
		}

		if (_(params.domElem).isObject()) {
			this.model2D = new Model2D(params.domElem);
			this.state = '2D';

			this.identifier = params.domElem.attr('id');
		}

		this.model3D = _(params.model3D).isObject() ? params.model3D : false;

		if (this.model3D) {
			this.state = '3D';
		}

		this.init();
	}

	BetweenObject.prototype.init = function() {
		var that = this;
		if (this.model2D) {
			this.model2D.click(function(model2D){
				radioSound.play();
				audioAmbient.play();
				webgl.moveCamera({
					position : new THREE.Vector3(-500, -400, 200),
					easing : 'outQuad',
					time : 800
				});
				that.switchState();
			});
		}
	};

	BetweenObject.prototype.switchState = function() {
		this.state === '3D' ? this.become2D() : this.become3D();
	};

	BetweenObject.prototype.become3D = function() {
		var that = this;
		this.state = '3D';

		if (!this.model3D) {
			webgl.addObject(this.model3D = this.init3DEquivalent(), this.identifier);
			this.model3D.hide();
			this.model3D.position.set(0,0, -80);
			this.model3D.startAnim('appear', function(){
				webgl.startPeriod('unstable1');
				webgl.startPeriod('unstable2');
				webgl.startPeriod('unstable3');
				webgl.startPeriod('unstable4');
				webgl.startPeriod('unstable5');
			}, function(){
				that.model2D.hide();
			});
		}

		this.model2D.pushIn3D();
	};

	BetweenObject.prototype.become2D = function() {
		this.state = '2D';

		this.model2D.comeBackIn2D();
	};

	BetweenObject.prototype.init3DEquivalent = function() {
		if (this.model2D) {
			return this.model2D.init3DEquivalent();
		}

		return false;
	};

	BetweenObject.prototype.update = function(deltaTime) {
		if (this.model2D) {
			this.model2D.update(deltaTime);
		}
		if(this.model3D){
			this.model3D.update(deltaTime);
		}
	};

	return BetweenObject;

}());