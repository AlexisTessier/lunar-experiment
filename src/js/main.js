'use strict';

var webgl, gui, counter = 0, layer2D, allObjects = [], impactTime = 0, effectScreen, timeAfterImpact = 0;
var endedImpact = false;
var wallImpact = 0;
var impactPosition = false;
var sphereUp = false;
var audioAmbient = null;
var animIntro = 0;
var radioSound = null;

var cameraMoveDone = 0;
var screenEndSound = null;

var renderFunction = function (webgl, deltaTime) {
    var wall = webgl.getObject('wallBetweenTwoWorlds');
    var modifSkybox = 0;
        if (wallImpact > 0) {
            timeAfterImpact = webgl.totalTime - impactTime;
            if (timeAfterImpact > 100) {
                modifSkybox = 0.1;
            }
            if (timeAfterImpact > 600) {
                modifSkybox = 0.2;
            }
            if (timeAfterImpact > 1000) {
                modifSkybox = 0.3;
            }
            if (timeAfterImpact > 1400) {
                modifSkybox = 0.4;
            }
            if (timeAfterImpact > 1900) {
                modifSkybox = 0.5;
            }
            if (timeAfterImpact > 2200) {
                modifSkybox = 0.6;
            }
            if (timeAfterImpact > 2800) {
                modifSkybox = 0.7;
            }
            if (timeAfterImpact > 3500) {
                modifSkybox = 0.8;
            }
            if (timeAfterImpact > 4000) {
                modifSkybox = 0.9;
            }
            if (timeAfterImpact > 5000) {
                modifSkybox = 1;
            }
            if (timeAfterImpact > 8000){
                modifSkybox = timeAfterImpact/7900;
            }
        }

        var maxDiviseur = 1800;

        var plusX = (this.deltaTime/(maxDiviseur-200))*modifSkybox;
        var plusY = (this.deltaTime/maxDiviseur)*modifSkybox;
        var plusZ = (this.deltaTime/(maxDiviseur-100))*modifSkybox;

        webgl.skybox.rotation.x += plusX;
        webgl.skybox.rotation.y += plusY;
        webgl.skybox.rotation.z += plusZ;

        /*wall.rotation.x += plusX;
        wall.rotation.y += plusY;*/
        wall.rotation.z += plusZ;

    /*---------*/

    var longestUnstable = webgl.getPeriod('unstable5');
    if (longestUnstable.start) {
        if (wallImpact === 0) {
            impactTime = webgl.totalTime;
        }
        var mainCharacter = webgl.getObject('main-character');
        wallImpact = 1;

        impactPosition = new THREE.Vector3(mainCharacter.position.x, mainCharacter.position.y, mainCharacter.position.z);
    };

    var modifImpact = timeAfterImpact/8000; 
    var ratioEffectScreen = _.random(8, 15)/120*(modifImpact > 1 ? modifImpact : 1);
    var screenDeform = webgl.getPeriod('screenEffect');
    //console.log(screenDeform)
    if (screenDeform.start) {
        ratioEffectScreen += screenDeform.easeFactor*screenDeform.easeFactor*20000;
    }

    //console.log(ratioEffectScreen)
    
    if (longestUnstable.loopCount > 0) {
        wallImpact++;
    }

    if (wallImpact > 0 && wallImpact < 2) {


        for (var i = 0; i < wall.mesh.geometry.vertices.length; i++) {
            var unstable = wall.periodsForVertices[i];

            if(unstable.loopCount < 1){
                var ratioUnstable = unstable.easeFactor;

                wall.mesh.material.attributes.ratio.value[i] = (ratioUnstable-0.5)*2;
                //wall.mesh.material.attributes.impact.value[i] = 1.0;
            }
        };
        //wall.mesh.material.attributes.impact.needsUpdate = true;

        THREE.ShaderLib['unstable'].uniforms.impact.value = 1.0;
        THREE.ShaderLib['unstable'].uniforms.impactPosition.value = impactPosition;
    }
    else{
        for (var i = 0; i < wall.mesh.geometry.vertices.length; i++) {
            wall.mesh.material.attributes.ratio.value[i] = wallImpact > 0 ? wall.mesh.material.attributes.ratio.value[i] : 0.6;
        }
        if (wallImpact === 2){
            sphereUp = true;
            wallImpact++;
            endedImpact = true;
        }
        //THREE.ShaderLib['unstable'].uniforms.impact.value = 0.0;
    }

    THREE.ShaderLib['unstable'].uniforms.endedImpact.value = endedImpact ? 1.0 : 0.0;

    /*var unstableConstant = webgl.getPeriod('unstableConstant');
    if (unstableConstant.start) {*/
        for (var i = 0; i < wall.mesh.geometry.vertices.length; i++) {
            var unstableConstant = wall.periodsConstantForVertices[i];

            if (wall.mesh.material.attributes.ratio.value[i] > 0.6) {
                wall.mesh.material.attributes.ratio.value[i] -= 0.0002*deltaTime;
            }
            if(wall.mesh.material.attributes.ratio.value[i] <= 0.6){
                wall.mesh.material.attributes.ratio.value[i] = 0.6;
            }

            /*if(endedImpact){
                wall.mesh.material.attributes.impactModif.value[i] = unstableConstant.easeFactor;
                //console.log(unstableConstant.easeFactor)
            }*/
        }

        //wall.mesh.material.attributes.impactModif.needsUpdate = endedImpact;
        
        wall.mesh.material.attributes.ratio.needsUpdate = true;
    //}

    _(allObjects).each( function( object, key, allObjects ) {
    	object.update(deltaTime);
    });

    if (webgl.totalTime > 600 && animIntro === 0) {
        $('#mainTitle').removeClass('disappear');

        animIntro++;
    }

    if (webgl.totalTime > 2000 && animIntro === 1) {
        audioAmbient.play();

        animIntro++;
    }

    if (webgl.totalTime > 6000 && animIntro === 2) {
        $('#blackCache').addClass('disappear');

        animIntro++;
    }

    if (webgl.totalTime > 7500 && animIntro === 3) {
        $('#main-character').addClass('here');
        $('#mainTitle').addClass('disappear');

        animIntro++;
    }

    if (webgl.totalTime > 15000 && animIntro === 4) {
        $('#blackCache, #mainTitle').addClass('hidden');

        audioAmbient.pause();

        animIntro++;
    };

    /*-----------------------------------------------*/

    if (timeAfterImpact > 600 && cameraMoveDone === 0) {
        /*webgl.moveCamera({
            position : new THREE.Vector3(-20, 200, 300),
            easing : 'linear',
            time : 250,
            lookAt : new THREE.Vector3(0, 0, 25)
        });*/

        cameraMoveDone++;
    }

    if (timeAfterImpact > 750 && cameraMoveDone === 1) {
        webgl.moveCamera({
            position : new THREE.Vector3(-450, 300, -10),
            easing : 'outQuad',
            time : 6500,
            lookAt : new THREE.Vector3(0, 0, 500)
        });

        cameraMoveDone++;
    }

    if (timeAfterImpact > 7700 && cameraMoveDone === 2) {
        webgl.moveCamera({
            position : new THREE.Vector3(-425, 280, 10),
            easing : 'linear',
            time : 200,
            lookAt : new THREE.Vector3(0, 0, 480)
        });
        
        cameraMoveDone++;
    }

    if (timeAfterImpact > 7900 && cameraMoveDone === 3) {
        webgl.moveCamera({
            position : new THREE.Vector3(-50, -65, 920),
            easing : 'outQuad',
            time : 5300,
            lookAt : new THREE.Vector3(0, 0, 0)
        });
        
        cameraMoveDone++;
    }

    if (timeAfterImpact > 14000 && cameraMoveDone === 4) {
        webgl.moveCamera({
            position : new THREE.Vector3(50, 60, 910),
            easing : 'outQuart',
            time : 5300,
            lookAt : new THREE.Vector3(0, 0, 0)
        });
        
        cameraMoveDone++;
    }

    if (timeAfterImpact > 17000 && cameraMoveDone === 5) {
        webgl.moveCamera({
            position : new THREE.Vector3(-60, -80, 1550),
            easing : 'linear',
            time : 12000,
            lookAt : new THREE.Vector3(0, 0, 0)
        });
        
        cameraMoveDone++;
    }

    if (timeAfterImpact > 33600 && cameraMoveDone === 6) {
        webgl.startPeriod('screenEnd');
        screenEndSound.play();
        audioAmbient.pause();

        cameraMoveDone++;
    }

    var screenEnd = webgl.getPeriod('screenEnd');
    if (screenEnd.start && !screenEnd.finish) {
        var multiplier = 150*screenEnd.easeFactor;
        ratioEffectScreen *= multiplier;
    }

    if (timeAfterImpact > 33900) {
        ratioEffectScreen = 1000;
        

        if (cameraMoveDone === 7) {
            $("#blackCache").removeClass('hidden').removeClass('disappear');
            cameraMoveDone++;
        }
    }

    effectScreen.render(webgl.scene, webgl.camera, ratioEffectScreen);
};


$(document).ready(init);

function getScreenEffectPeriod () {
    var duration = _.random(400, 800);
    return {
        duration : duration,
        loop : true,
        easing : 'inOutElastic',
        reverse : true,
        endAction : initPeriodScreenEffect,
        maxLoopCount : 1
    }
}

function initPeriodScreenEffect () {
    var duration = _.random(300, 5000);
    webgl.addPeriod('screenEffect', getScreenEffectPeriod());
    webgl.addPeriod('screenEffectDelay', {
        duration : duration,
        endAction : function (period) {
            webgl.startPeriod('screenEffect');
        }
    });

    webgl.startPeriod('screenEffectDelay');
}

function init(){
	layer2D = $('#layer-2d');

    var container3D = $('.three').first();
    webgl = new Webgl(window.innerWidth, window.innerHeight, container3D);

    webgl.addPeriod('screenEnd', {
        duration : 400,
        easing : 'outElastic'
    });

    /*webgl.addPeriod('unstableConstant1', {duration : 3500, loop : true, easing : 'inOutCubic', reverse : true});
    webgl.addPeriod('unstableConstant2', {duration : 2000, loop : true, easing : 'inOutCubic', reverse : true});
    webgl.addPeriod('unstableConstant3', {duration : 800, loop : true, easing : 'inOutCubic', reverse : true});
    webgl.addPeriod('unstableConstant4', {duration : 1200, loop : true, easing : 'inOutCubic', reverse : true});
    webgl.addPeriod('unstableConstant5', {duration : 900, loop : true, easing : 'inOutCubic', reverse : true});

    webgl.startPeriod('unstableConstant1');
    webgl.startPeriod('unstableConstant2');
    webgl.startPeriod('unstableConstant3');
    webgl.startPeriod('unstableConstant4');
    webgl.startPeriod('unstableConstant5');*/

    webgl.addPeriod('unstable1', {duration : 1500, loop : true, easing : 'outElastic', reverse : true});
    webgl.addPeriod('unstable2', {duration : 1200, loop : true, easing : 'outElastic', reverse : true});
    webgl.addPeriod('unstable3', {duration : 800, loop : true, easing : 'outElastic', reverse : true});
    webgl.addPeriod('unstable4', {duration : 1000, loop : true, easing : 'outElastic', reverse : true});
    webgl.addPeriod('unstable5', {duration : 1800, loop : true, easing : 'outElastic', reverse : true});

    initPeriodScreenEffect();

    effectScreen = new THREE.ParallaxBarrierEffect(webgl.renderer);
    effectScreen.setSize( window.innerWidth, window.innerHeight );

    container3D.append(webgl.renderer.domElement);

    //gui = new dat.GUI();
    //gui.close();

    $(window).on('resize', resizeHandler);

    webgl.setRenderFunction(renderFunction);

    var floor = webgl.addObject(new Plane({
        width : 1000,
        height : 1000,
        textureName : 'water.jpg',
        widthSegments : 180,
        heightSegments : 150}), 'wallBetweenTwoWorlds');
    floor.position.set(0, 0, -60);

    animate();

    var mainCharacter = new BetweenObject({
    	domElem : $('#main-character')
    });

    allObjects.push(mainCharacter);

    initAudio();
}

function resizeHandler() {
    webgl.resize(window.innerWidth, window.innerHeight);
}

function animate() {
    requestAnimationFrame(animate);
    webgl.render();
}

function initAudio() {
    audioAmbient = new Audio('./moonwalk.mp3');
    radioSound = new Audio('./radiosound.mp3');
    screenEndSound = new Audio('./screenend.mp3');
    radioSound.volume = 0.25;
    audioAmbient.volume = 1;
    screenEndSound.volume = 0.56;
}